const dbService = require('../../services/db.service');
const userService = require('../../api/user/user.service');
const ObjectId = require('mongodb').ObjectId;
const logger = require('../../services/logger.service');

const asyncLocalStorage = require('../../services/als.service');
// var mongoose = require('mongoose');
var _ = require('lodash');

async function query() {
  try {
    const store = asyncLocalStorage.getStore();
    const { userId } = store;
    const criteria = _buildCriteria(userId);
    const collection = await dbService.getCollection('board');
    const boards = await collection
      .aggregate([
        {
          $match: criteria,
        },
        {
          $lookup: {
            localField: 'createdBy',
            from: 'user',
            foreignField: '_id',
            as: 'createdBy',
          },
        },
        {
          $unwind: '$createdBy',
        },
        {
          $lookup: {
            localField: 'members',
            from: 'user',
            foreignField: '_id',
            as: 'members',
          }
        },
        {
          $lookup: {
            from: 'user',
            localField: 'lists.cards.members',
            foreignField: '_id',
            as: 'lists.cards.members1'
          }
        },


        {
          $project: {
            'createdBy.password': 0,
            'createdBy.starredBoardsIds': 0,
            'members.password': 0,
            'members.starredBoardsIds': 0,
          },
        },
      ])
      .toArray();
    return boards;
  } catch (err) {
    logger.error('cannot find boards', err);
    throw err;
  }
}

async function getById(boardId) {
  try {
    const collection = await dbService.getCollection('board');
    const board = await collection
      .aggregate([
        {
          $match: { _id: ObjectId(boardId) },
        },

        {
          $lookup: {
            localField: 'createdBy',
            from: 'user',
            foreignField: '_id',
            as: 'createdBy',
          },
        },
        {
          $unwind: '$createdBy',
        },
        {
          $lookup: {
            localField: 'members',
            from: 'user',
            foreignField: '_id',
            as: 'members',
          },
        },




        // *********************************
        {
          $unwind: '$activities'

        }, {

          $unwind: '$activities.createdBy'

        },





        {
          $lookup: {
            from: 'user',
            localField: 'activities.createdBy',
            foreignField: '_id',
            as: 'activities.createdBy'
          }
        },
        {

          $unwind: '$activities.createdBy'

        },


        {
          $group: {
            _id: '$_id',
            title: {
              $first: '$title'
            },
            createdAt: {
              $first: '$createdAt'
            },
            createdBy: {
              $first: '$createdBy'
            },
            activities: {
              $push: '$activities'
            },
            members: {
              $first: '$members'
            },
            style: {
              $first: '$style'
            },
            labels: {
              $first: '$labels'
            },
            archive: {
              $first: '$archive'
            },
            isFullLabels: {
              $first: '$isFullLabels'
            },
            lists: {
              $first: '$lists'
            }
          }
        },





        // ******************************



        {
          $project: {
            'createdBy.password': 0,
            'createdBy.starredBoardsIds': 0,
            'createdBy.notifications': 0,


            'members.password': 0,
            'members.notifications': 0,
            'members.starredBoardsIds': 0,


            'activities.createdBy.password': 0,
            'activities.createdBy.notifications': 0,
            'activities.createdBy.mentions': 0,
            'activities.createdBy.createdBy.starredBoardsIds': 0,
            // 'members.password': 0,
            // 'members.starredBoardsIds': 0,

          },
        },
      ])
      .toArray();

    // console.log( board);



    if (board[0].lists && board[0].lists.length) {
      console.log('list')

      board[0].lists = board[0].lists.map(list => {
        // console.log(activities);


        if (list.cards && list.cards.length) {
          console.log('list.cards')
          list.cards.map(card => {
            if (card.members && card.members.length) {
              console.log('list.cards.members')

              card.members = card.members.map(member => {
                console.log('list.cards.members ==========')

                if (board[0].members.find((member1) => String(member1._id) === String(member))) {
                  console.log('list.cards.members ***********************')
                
                  return board[0].members.find((member1) => String(member1._id) === String(member))
                }
                return member



              })

            }
            return card

          })

        }
        return list
      }
      )
    }

    // console.log(board[0]);

    return board[0];
  } catch (err) {
    logger.error(`Cannot get board by Id ${boardId}`, err);
    throw err;
  }
}

async function remove(boardId) {
  try {
    const collection = await dbService.getCollection('board');
    await collection.deleteOne({ _id: ObjectId(boardId) });
    return boardId;
  } catch (err) {
    logger.error(`Cannot remove board ${boardId}`, err);
    throw err;
  }
}

async function add(board) {
  const defaultLabels = [
    { id: _makeId(), title: '', color: 'green' },
    { id: _makeId(), title: '', color: 'yellow' },
    { id: _makeId(), title: '', color: 'orange' },
    { id: _makeId(), title: '', color: 'red' },
    { id: _makeId(), title: '', color: 'purple' },
    { id: _makeId(), title: '', color: 'blue' },
  ];
  try {
    const { title, style, lists = [], labels = defaultLabels } = board;
    // when creating through template - there might be lists and labels,
    // normal creation - lists are empty array and labels are default labels.
    const store = asyncLocalStorage.getStore();
    const { userId } = store;
    const boardToAdd = {
      title,
      style,
      createdBy: ObjectId(userId),
      members: [ObjectId(userId)],
      createdAt: Date.now(),
      labels,
      isFullLabels: false,
      lists,
      archive: { lists: [], cards: [] },
      activities: [

        {
          id: '999999999999',
          createdBy: 
          ObjectId(userId)
          ,
          createdAt: Date.now()
        }

      ],
    };
    const collection = await dbService.getCollection('board');
    await collection.insertOne(boardToAdd);
    return boardToAdd;
  } catch (err) {
    logger.error('cannot insert board', err);
    throw err;
  }
}

async function update(board) {






  try {

    const board2 = _.cloneDeep(board);

    // console.log("activities ==> 1", board2.activities[2].createdBy);



    let listsToupdate

    if (board.lists.length) {
      listsToupdate = board.lists.map((list) => {
        if (list.cards.length) {
          list.cards = list.cards.map((card) => {

            if (card.members) {
              // logger.error(card.members);
              if (card.members.length) {
                card.members = card.members.map((member1) =>
                  ObjectId(member1._id)
                )
              }
            }
            return card


          }

          )
          // return list

        }

        return list
      },



      )
    } else {

      listsToupdate = []
    }



    const boardToSave = {
      ...board,

      createdBy: ObjectId(board.createdBy._id),
      members: board.members.map(member => ObjectId(member._id)),
      lists: listsToupdate
      ,


      activities:



        board.activities.map(activity => {
          modefiedActivity = activity
          modefiedActivity.createdBy = ObjectId(activity.createdBy._id)
          return modefiedActivity
        }
        ),


      _id: ObjectId(board._id),
    };
    // console.log("activities ==> 2",board2.activities[2].createdBy);

    const collection = await dbService.getCollection('board');
    await collection.updateOne({ _id: boardToSave._id }, { $set: boardToSave });
    // console.log("activities ==> 3",board2.activities[2].createdBy);

    return board2;
  } catch (err) {
    logger.error(`Cannot update board ${board._id}`, err);
    throw err;
  }
}

function _buildCriteria(userId) {
  const criteria = {};
  if (userId) criteria.members = { $in: [ObjectId(userId)] };
  return criteria;
}

function _makeId(length = 6) {
  var txt = '';
  var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

  for (var i = 0; i < length; i++) {
    txt += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return txt;
}

module.exports = {
  query,
  remove,
  add,
  update,
  getById,
};
